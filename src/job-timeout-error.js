'use strict'

class JobTimeoutError extends Error {
  constructor (args) {
    super(args)
    this.name = 'JobTimeoutError'
  }
}

module.exports = JobTimeoutError
