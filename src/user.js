'use strict'

const defaultstore = require('rc')('htr').users

class User {
  constructor (name, hash) {
    this.name = name
    this.hash = hash
  }

  static getAllUsers (store = defaultstore) {
    if (!store) return []
    return store.map(x => new User(x.name, x.hash))
  }
}

module.exports = User
