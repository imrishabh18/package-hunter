'use strict'

const debug = require('debug')('pkgs:server')
const express = require('express')
const mkdirp = require('mkdirp')
const FalcoClient = require('./falco-client')
const AlertFilter = require('./alert-filter.js')
const printBanner = require('./banner')
const setupRoutes = require('./route.js')

const opts = {
  workdir: '/tmp/package-hunter-workdir',
  jobStore: {},
  pendingContainer: {}
}
const PORT = 3000

// expressjs config
const app = express()
setupRoutes(app, opts)

_setupFalcoListener()

function _setupFalcoListener () {
  const falco = new FalcoClient()
  const falcoStream = falco.subscribe()

  falcoStream.on('data', function (alert) {
    const conId = alert.output_fields && alert.output_fields['container.id']
    debug(`received an alert for container ${conId}`)
    if (!conId) {
      debug(`Received alert without container id: ${alert}`)
      return
    }

    const reqId = opts.pendingContainer[conId]
    if (!reqId) {
      debug(`Could not find request id for container with id ${conId}`)
      return
    }

    const filter = new AlertFilter(alert)
    if (filter.isDiscardable()) {
      debug(`Discarding alert: ${alert.output}`)
      return
    }

    opts.jobStore[reqId].result.push(alert)
  })
  falcoStream.on('end', end => {
    debug('falco client connection unexpectedly closed')
    clearInterval(streamWriteTicker)
    _setupFalcoListener()
  })
  falcoStream.on('error', err => {
    debug('falco client encountered an error: ' + err)
  })
  falcoStream.on('status', function (status) {
    debug('falco status: %O', status)
  })

  // Write an empty message to the stream every 100ms in order to receive events.
  // It's currently unclear why this is needed, but it's assumed to be needed to tell Falco
  // That a process is subscribed and ready to accept events. If this assumption is wrong, please
  // don't hesitate to open an Issue or MR on Package Hunter! :)
  const streamWriteTicker = setInterval(() => {
    falcoStream.write({ data: {} })
  }, 100)
}

mkdirp(opts.workdir)
app.listen(PORT, () => {
  debug(`Dependency Monitor listening at http://localhost:${PORT}`) // DevSkim: ignore DS137138
  printBanner()
})
