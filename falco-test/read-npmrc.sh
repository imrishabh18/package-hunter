#!/bin/bash

echo "running $(basename $0)"

# see https://docs.npmjs.com/files/npmrc

cat .npmrc > /dev/null 2>&1  # per-project config
cat ~/.npmrc > /dev/null 2>&1  # per-user config
cat /etc/.npmrc > /dev/null 2>&1  # global config file
NPM_PATH=`which /Users/da/.nvm/versions/node/v12.16.1/bin/npm`
cat $NPM_PATH/.npmrc > /dev/null 2>&1 
